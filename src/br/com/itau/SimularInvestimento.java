package br.com.itau;

public class SimularInvestimento {

    private double valor;
    private int meses;

    public double CalcularAcumulado() {

        double rendimento = this.valor * 0.07;
        double acumulado = rendimento * this.meses;

        return acumulado;

    }

    public double getValor() {
        return valor;
    }

    public int getMeses() {
        return meses;
    }
    public void setValor(double valor) {
        this.valor=valor;
    }

    public void setMeses(int meses) {
        this.meses=meses;
    }
}
