package br.com.itau;

import java.sql.SQLOutput;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        SimularInvestimento simularInvestimento = new SimularInvestimento();
        Scanner sc = new Scanner(System.in);

        System.out.println("Quanto você deseja investir?");
        System.out.print("Resposta: ");
        simularInvestimento.setValor(sc.nextDouble());

        System.out.println("Por quantos meses você deseja investir seus R$"+simularInvestimento.getValor()+" ?");
        System.out.print("Resposta: ");
        simularInvestimento.setMeses(sc.nextInt());

        double resultado=simularInvestimento.CalcularAcumulado();

        System.out.printf("Seu valor acumulado dos R$%.2f será de R$%.2f no periodo de "+simularInvestimento.getMeses()+" meses!",simularInvestimento.getValor(),resultado);
    }
}
